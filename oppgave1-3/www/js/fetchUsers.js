function fetchUsers() {
    fetch('api/fetchUsers.php')
        .then(res => res.json())
        .then(users => {
            console.log(users)
            const userUL = document.querySelector('.users ul');
            userUL.innerHTML = '';
            users.forEach(user => {
                const userLI = document.createElement('LI');
                userLI.setAttribute('data-uid', user.uid);
                userLI.innerHTML += `<span>${user.firstName} ${user.lastName}</span><span>${user.uname}</span>`;
                userLI.addEventListener("click", () => {
                    editUser(user.uid);
                })
                userUL.appendChild(userLI);
            })

        })
}

fetchUsers();
