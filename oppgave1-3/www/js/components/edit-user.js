import { LitElement, html, css } from "../../node_modules/lit-element/lit-element.js";

class EditUser extends LitElement {
  static get properties() {
    return {
      user: { type: Object }
    };
  }

  // din kode her

  updateUser(e) {
    e.preventDefault();
    let body = new FormData(e.target);
    body.append("uid", this.user.uid);
    fetch(`api/updateUser.php`, {
      method: "POST",
      body: body
    }).then(res => res.json())
      .then(data => console.log(data.status))
  }
  render() {
    return html`

    <form @submit="${(e) =>
        this.updateUser(e)
      }">
    <label for="uname">Brukernavn</label><input value="${this.user.uname}" type="email" id="uname" name="uname" required
        pattern=".{8,}" placeholder="hans@hansen.no" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
        title="Gyldig e-post adresse"><br>
    <label for="oldpwd">Eksisterende passord</label><input type="password" id="oldpwd" name="oldpwd"
        pattern=".{8,}" title="Seks eller flere tegn"><br>
    <label for="pwd">Passord</label>
    <input type="password" id="pwd" name="pwd" pattern=".{8,}"
        title="Seks eller flere tegn"><br>


    <label for="pwd1">Bekreft passord</label>
    <input type="password" id="pwd1" name="pwd1"  title="Seks eller flere tegn"><br>

    <label for="firstName">Fornavn</label>
    <input value="${this.user.firstName}" type="text" id="firstName" name="firstName" required
        pattern=".{2,}" title="To eller flere tegn"><br>


    <label for="lastName">Etternavn</label>
    <input value="${this.user.lastName}" type="text" id="lastName" name="lastName" required
        pattern=".{4,}" title="Fire eller flere tegn"><br>
    <input type="submit" value="Oppdater informasjon">
    <div class="response"></div>
</form>
  `
  }


}
customElements.define('edit-user', EditUser);
