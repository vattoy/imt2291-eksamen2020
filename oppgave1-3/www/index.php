<?php
require_once '../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('./templates');
$twig = new \Twig\Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));

if (!isset($_POST['uname'])) {  // Show form to create user
  echo $twig->render('addUser.html', array());
} else {                        // User data exists, create new user
  require_once 'classes/DB.php';
  $db = DB::getDBConnection();
  $err['code'] = 1;
  $err['hint'] = 'Et av feltene er ikke fylt ut';
  // Check to see if all fields are filled in
  if (isset($_POST['uname'])&&isset($_POST['pwd'])&&isset($_POST['firstName'])&&isset($_POST['lastName'])) {
    $err['code'] = 0;
    if (!preg_match("/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/",$_POST['uname'])) {  // Check pwd
      $err['code'] = 1;
      $err['hint'] = 'Ugyldig e-post addresse';
    } else if (strlen($_POST['pwd'])<8) {
      $err['code'] = 1;
      $err['hint'] = 'Passord for kort';
    } else if (strlen($_POST['firstName'])<2) {
      $err['code'] = 1;
      $err['hint'] = 'Fornavnet er for kort';
    } else if (strlen($_POST['lastName'])<4) {
      $err['code'] = 1;
      $err['hint'] = 'Etternavnet er for kort';
    }
  }
  if ($err['code']==1) {  // One or more fields are wrong
    echo $twig->render('addUser.html', $err);
  } else {                // All fields are OK
    $stmt = $db->prepare('INSERT INTO user (uname, pwd, firstName, lastName) VALUES(?, ?, ?, ?)');
    $stmt->execute(array($_POST['uname'], password_hash($_POST['pwd'], PASSWORD_DEFAULT), $_POST['firstName'], $_POST['lastName']));
    if ($stmt->rowCount()==1) { // User added to DB
      echo $twig->render('userAdded.html', $_POST);
    } else {                    // User not added to DB
      $err['code'] = 1;
      $err['hint'] = 'Kunne ikke legge til bruker i databasen.';
      echo $twig->render('addUser.html', $err);
    }
  }
}

